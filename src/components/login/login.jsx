import React, {useState} from 'react';
import axios from 'axios';
import Auth from '../../servants/auth';
import {Redirect} from 'react-router-dom';
import { Button, Input, Grid } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

function Login (props){

  const [loginError, setLoginError] = useState(false);
  const [user, setUser] = useState('');
  const [pw, setPw] = useState('');
  const [isAuthenticated, setIsAuthenticated] = useState(Auth.isAuthenticated());

  const login = () => {

    axios.post('http://localhost:3000/login', {user,pw}).then((res) => {
      let token = res.data.token;
      if(token){
        localStorage.setItem('token',token);
        setIsAuthenticated(true);
      } else {
        setLoginError(true);
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  const updateUser = (evt) => {
    setUser(evt.target.value);
  }
  const updatePw = (evt) => {
    setPw(evt.target.value);
  }

  const loginScreen = () => {
    return (      
      <Grid>
        <h1>time to login boiii</h1>

          <div>
            <Input error label="user name" helpertext="user name is required" required placeholder="user name" id="user" value={user} onChange={updateUser}></Input>
          </div>
          <div>
            <Input error required placeholder="passsword" label="password" helpertext="password is required" type="password" id="pw" value={pw} onChange={updatePw}></Input>
          </div>
          <Button variant="contained" color="primary" onClick={login}>Login</Button>
          <div>
            {loginError ? <Alert variant="filled" severity="error">Error with login, please try again</Alert> : ''}
          </div>

      </Grid>
    );
  }
  return (!isAuthenticated ? loginScreen() : <Redirect to="/"></Redirect>);
}
export default Login;