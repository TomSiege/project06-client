class Auth {
  constructor(){
    this.authenticated = false;
  }
  isAuthenticated() {
    return localStorage.getItem('token') !== null;
  }
}

export default new Auth();