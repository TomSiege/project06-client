import './App.css';
import Login from '../src/components/login/login'
import Error from '../src/components/error/error'
import { ProtectedRoute } from '../src/components/protected_route/protected_route'
import Dashboard from '../src/components/dashboard/dashboard'
import { Switch, Route } from 'react-router-dom';

function App() {
   
  return (
    <div className="App">
      <Switch>
        <ProtectedRoute path="/" component={ Dashboard } exact></ProtectedRoute>
        <Route path="/login" component={ Login }></Route>
        <Route component={ Error }></Route>
      </Switch>
    </div>
  );
}

export default App;
